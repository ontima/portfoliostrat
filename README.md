# PortfolioStrat MEAN Programming Exercise

Application is available at https://oc-portfoliostrat.herokuapp.com/

### Prerequisities

Mongo must running 

### Installing

Download the code and run 'npm install' followed by 'npm start'
Application will be available at http://localhost:3000/

## Running the tests

Run: npm test

### Screenshots
![Screen Shot 2016-10-07 at 12.10.41 AM.png](https://bitbucket.org/repo/kE7Loy/images/1143203218-Screen%20Shot%202016-10-07%20at%2012.10.41%20AM.png)
![Screen Shot 2016-10-07 at 12.10.22 AM.png]
(https://bitbucket.org/repo/kE7Loy/images/1408410753-Screen%20Shot%202016-10-07%20at%2012.10.22%20AM.png)
![Screen Shot 2016-10-07 at 12.09.48 AM.png](https://bitbucket.org/repo/kE7Loy/images/2378925988-Screen%20Shot%202016-10-07%20at%2012.09.48%20AM.png)